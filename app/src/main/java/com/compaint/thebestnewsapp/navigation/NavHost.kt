package com.compaint.thebestnewsapp.navigation

import androidx.compose.runtime.Composable
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.compaint.thebestnewsapp.presentation.ui.detail.DetailScreen
import com.compaint.thebestnewsapp.presentation.ui.news.NewsScreen

@Composable
fun AppNavHost(navController: NavHostController) {
    NavHost(navController = navController, startDestination = NavDestinations.News.url) {
        composable(route = NavDestinations.News.url) {
            NewsScreen(navController)
        }
        composable(route = NavDestinations.Details.url) {
            DetailScreen(navController)
        }
    }
}

sealed class NavDestinations(val url: String) {
    object News : NavDestinations("News")
    object Details : NavDestinations("details?new={link}") {
        fun uri(link: String) = url.replace("{link}", link)
        fun getLink(savedStateHandle: SavedStateHandle): String? = savedStateHandle["link"]
    }
}
