package com.compaint.thebestnewsapp.di

import com.compaint.thebestnewsapp.data.api.RestApi
import com.compaint.thebestnewsapp.data.repository.NewsRepository
import com.compaint.thebestnewsapp.data.repository.NewsRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    companion object {

        @Provides
        @Singleton
        fun provideRestApi(
            retrofit: Retrofit
        ): RestApi {
            return retrofit.create(RestApi::class.java)
        }
    }

    @Binds
    abstract fun bindNewsRepository(repo: NewsRepositoryImpl): NewsRepository
}