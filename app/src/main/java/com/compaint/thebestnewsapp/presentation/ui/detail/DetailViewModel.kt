package com.compaint.thebestnewsapp.presentation.ui.detail

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.compaint.thebestnewsapp.navigation.NavDestinations
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _viewState = MutableStateFlow(DetailViewState())
    val viewState = _viewState.asStateFlow()

    init {
        val link = NavDestinations.Details.getLink(savedStateHandle)
            ?: throw Exception("DetailViewModel without link")

        _viewState.update { it.copy(link = link) }
    }
}
