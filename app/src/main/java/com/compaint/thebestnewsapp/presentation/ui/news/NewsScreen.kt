package com.compaint.thebestnewsapp.presentation.ui.news

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.paging.LoadState
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.items
import coil.compose.rememberAsyncImagePainter
import com.compaint.thebestnewsapp.R
import com.compaint.thebestnewsapp.navigation.NavDestinations
import com.compaint.thebestnewsapp.presentation.model.ArticleModel
import com.google.accompanist.placeholder.PlaceholderHighlight
import com.google.accompanist.placeholder.material.fade
import com.google.accompanist.placeholder.material.placeholder

@Composable
fun NewsScreen(
    navController: NavController,
    viewModel: NewsViewModel = hiltViewModel()
) {
    val recipes = viewModel.getNews().collectAsLazyPagingItems()

    Box {
        Text(
            text = stringResource(id = R.string.app_name),
            style = MaterialTheme.typography.h5.copy(color = Color.Black),
            modifier = Modifier
                .background(color = Color.LightGray)
                .padding(16.dp)
                .fillMaxWidth()
        )
    }

    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = 64.dp)
            .padding(horizontal = 16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        when (val state = recipes.loadState.prepend) {
            is LoadState.NotLoading -> Unit
            is LoadState.Loading -> {
                Loading()
            }
            is LoadState.Error -> {
                Error(message = state.error.message ?: "")
            }
        }
        when (val state = recipes.loadState.refresh) {
            is LoadState.NotLoading -> Unit
            is LoadState.Loading -> {
                Loading()
            }
            is LoadState.Error -> {
                Error(message = state.error.message ?: "")
            }
        }
        items(
            items = recipes,
            key = { it.publishedAt }
        ) {
            NewsRow(
                articleModel = it,
                onClick = {
                    navController.navigate(
                        NavDestinations.Details.uri(it?.url ?: "")
                    )
                }
            )
        }
        when (val state = recipes.loadState.append) {
            is LoadState.NotLoading -> Unit
            is LoadState.Loading -> {
                Loading()
            }
            is LoadState.Error -> {
                Error(message = state.error.message ?: "")
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun NewsRow(
    articleModel: ArticleModel?,
    onClick: () -> Unit
) {
    Spacer(modifier = Modifier.height(8.dp))
    Card(
        shape = RoundedCornerShape(8.dp),
        elevation = 4.dp,
        onClick = { onClick.invoke() }
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                painter = rememberAsyncImagePainter(articleModel?.urlToImage),
                contentDescription = null,
                modifier = Modifier
                    .placeholder(
                        visible = articleModel == null,
                        highlight = PlaceholderHighlight.fade(),
                    )
                    .size(64.dp)
            )
            Spacer(modifier = Modifier.width(16.dp))
            Column(
                modifier = Modifier.weight(1f)
            ) {
                Text(
                    text = articleModel?.title ?: "",
                    maxLines = 2,
                    modifier = Modifier
                        .placeholder(
                            visible = articleModel == null,
                            highlight = PlaceholderHighlight.fade(),
                        )
                        .fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(8.dp))
                Text(
                    text = articleModel?.description ?: "",
                    maxLines = 2,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier
                        .placeholder(
                            visible = articleModel == null,
                            highlight = PlaceholderHighlight.fade(),
                        )
                        .fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(8.dp))
                Text(
                    text = articleModel?.publishedAt ?: "",
                    maxLines = 1,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier
                        .placeholder(
                            visible = articleModel == null,
                            highlight = PlaceholderHighlight.fade(),
                        )
                        .fillMaxWidth()
                )
            }
        }
    }
    Spacer(modifier = Modifier.height(8.dp))
}


private fun LazyListScope.Loading() {
    item {
        CircularProgressIndicator(modifier = Modifier.padding(16.dp))
    }
}

private fun LazyListScope.Error(
    message: String
) {
    item {
        Text(
            text = message,
            style = MaterialTheme.typography.h6,
            color = MaterialTheme.colors.error
        )
    }
}
