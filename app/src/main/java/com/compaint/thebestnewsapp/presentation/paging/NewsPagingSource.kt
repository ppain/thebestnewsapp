package com.compaint.thebestnewsapp.presentation.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.compaint.thebestnewsapp.data.mapper.toModel
import com.compaint.thebestnewsapp.data.repository.NewsRepository
import com.compaint.thebestnewsapp.presentation.model.ArticleModel

class NewsPagingSource(
    private val newsRepository: NewsRepository
) : PagingSource<Int, ArticleModel>() {

    override fun getRefreshKey(state: PagingState<Int, ArticleModel>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ArticleModel> =
        try {
            val page = params.key ?: 0
            val size = params.loadSize
            val from = page * size
            val data = newsRepository.getEverything(page = from + 1, pageSize = size).toModel()
            if (params.placeholdersEnabled) {
                val itemsAfter = data.totalResults - from + data.articles.size
                LoadResult.Page(
                    data = data.articles,
                    prevKey = if (page == 0) null else page - 1,
                    nextKey = if (data.articles.isEmpty()) null else page + 1,
                    itemsAfter = if (itemsAfter > size) size else itemsAfter,
                    itemsBefore = from
                )
            } else {
                LoadResult.Page(
                    data = data.articles,
                    prevKey = if (page == 0) null else page - 1,
                    nextKey = if (data.articles.isEmpty()) null else page + 1
                )
            }
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
}
