package com.compaint.thebestnewsapp.presentation.ui.news

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.compaint.thebestnewsapp.data.repository.NewsRepository
import com.compaint.thebestnewsapp.presentation.paging.NewsPagingSource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class NewsViewModel @Inject constructor(
    private val recipesRepository: NewsRepository
) : ViewModel() {

    companion object {
        const val PAGE_SIZE_COUNT = 10
    }

    fun getNews() = Pager(
        pagingSourceFactory = { NewsPagingSource(recipesRepository) },
        config = PagingConfig(pageSize = PAGE_SIZE_COUNT)
    ).flow.cachedIn(viewModelScope)
}
