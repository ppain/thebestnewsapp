package com.compaint.thebestnewsapp.presentation.ui.detail

data class DetailViewState(
    val link: String = ""
)