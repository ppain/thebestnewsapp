package com.compaint.thebestnewsapp.presentation.model

data class ArticlesModel(
    val totalResults: Int,
    val articles: List<ArticleModel>
)

data class ArticleModel(
    val title: String,
    val description: String,
    val url: String,
    val urlToImage: String,
    val publishedAt: String
)