package com.compaint.thebestnewsapp.data.api

import com.compaint.thebestnewsapp.data.dto.ArticlesDto
import retrofit2.http.GET
import retrofit2.http.Query

interface RestApi {

    companion object {
        const val BASE_URL = "https://newsapi.org"
        const val API_KEY = "a1efdef1826c415c8d5ee4aaa5452724"
    }

    @GET("v2/everything")
    suspend fun getEverything(
        @Query("domains") domains: String,
        @Query("sortBy") sortBy: String,
        @Query("page") page: Int,
        @Query("pageSize") pageSize: Int,
    ): ArticlesDto

    @GET("v2/top-headlines")
    suspend fun getTopHeadline(
        @Query("country") country: String,
        @Query("page") page: Int,
        @Query("pageSize") pageSize: Int,
    ): ArticlesDto
}
