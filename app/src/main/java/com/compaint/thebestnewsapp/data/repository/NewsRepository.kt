package com.compaint.thebestnewsapp.data.repository

import com.compaint.thebestnewsapp.data.dto.ArticlesDto

interface NewsRepository {

    suspend fun getEverything(page: Int, pageSize: Int): ArticlesDto
    suspend fun getTopHeadline(): ArticlesDto
}
