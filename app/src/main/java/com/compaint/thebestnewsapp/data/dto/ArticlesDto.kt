package com.compaint.thebestnewsapp.data.dto

import kotlinx.serialization.Serializable

@Serializable
data class ArticlesDto(
    val status: String,
    val totalResults: Int,
    val articles: List<ArticleDto>
)

@Serializable
data class ArticleDto(
    val title: String,
    val description: String,
    val url: String,
    val urlToImage: String,
    val publishedAt: String
)
