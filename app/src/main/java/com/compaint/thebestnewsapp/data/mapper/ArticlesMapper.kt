package com.compaint.thebestnewsapp.data.mapper

import com.compaint.thebestnewsapp.data.dto.ArticleDto
import com.compaint.thebestnewsapp.data.dto.ArticlesDto
import com.compaint.thebestnewsapp.presentation.model.ArticleModel
import com.compaint.thebestnewsapp.presentation.model.ArticlesModel
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

fun ArticlesDto.toModel(): ArticlesModel = ArticlesModel(
    totalResults = totalResults,
    articles = articles.map { it.toModel() }
)

fun ArticleDto.toModel(): ArticleModel = ArticleModel(
    title = title,
    description = description,
    url = url,
    urlToImage = urlToImage,
    publishedAt = publishedAt.parseIsoDateTime(),
)

fun String.parseIsoDateTime(): String {
    val isoDateFormatter = DateTimeFormatter.ISO_INSTANT.withZone(ZoneId.systemDefault())
    val dateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL).withZone(ZoneId.systemDefault())

    val isoDateTime = LocalDateTime.parse(this, isoDateFormatter)
    return isoDateTime.format(dateTimeFormatter)
}
