package com.compaint.thebestnewsapp.data.repository

import com.compaint.thebestnewsapp.data.api.RestApi
import com.compaint.thebestnewsapp.data.dto.ArticlesDto
import javax.inject.Inject

class NewsRepositoryImpl @Inject constructor(
    private val restApi: RestApi
) : NewsRepository {

    companion object {
        const val DEFAULT_COUNTRY = "us"
        const val DEFAULT_DOMAINS = "techcrunch.com"
        const val DEFAULT_SORT = "popularity"
        const val DEFAULT_TOP_NEWS_SIZE = 1
        const val DEFAULT_TOP_NEWS_PAGE = 0
    }

    override suspend fun getEverything(page: Int, pageSize: Int): ArticlesDto {
        return restApi.getEverything(
            domains = DEFAULT_DOMAINS,
            sortBy = DEFAULT_SORT,
            page = page,
            pageSize = pageSize
        )
    }

    override suspend fun getTopHeadline(): ArticlesDto {
        return restApi.getTopHeadline(
            country = DEFAULT_COUNTRY,
            page = DEFAULT_TOP_NEWS_PAGE,
            pageSize = DEFAULT_TOP_NEWS_SIZE
        )
    }
}
